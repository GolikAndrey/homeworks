
document.getElementById('clickMe').onclick = function() {

    function fibo (number) {
        if (number === 0){
            return 0;
        } else if (number === 1){
            return 1;
        } else {
            let f1 = 0, f2 = 1, f3;
            for (let i = 1; i < number; i++) {
                f3 = f1 + f2;
                f1 = f2;
                f2 = f3;
            }
            return f2;
        }
    }
    function fiboMinus (number) {
        if (number === 0){
            return 0;
        } else if (number === -1){
            return 1;
        } else {
            let f1, f2 =1, f3=0;
            for (let i = number+1; i < 0; i++) {
                f1 = f3 - f2;
                f3 = f2;
                f2 = f1;
            }
            return f1;
        }
    }

    let userNumber = +prompt('Enter number:');
    while((userNumber.toFixed()!=userNumber) || isNaN(userNumber)) {
        userNumber = +prompt('Enter number AGAIN', '');
    }
    if (userNumber > 0){
        alert(`Value of Fibonacci number is: ${fibo(userNumber)}`);
    } else {
        alert(`Value of Fibonacci number is: ${fiboMinus(userNumber)}`);
    }
};