let time = 0;
let running = 0;
let tenths=0, minutes=0, seconds=0, hours=0;
function startPause(){
    if(running === 0){
        running = 1;
        increment();
        document.getElementById("start").innerHTML = "<i class=\"far fa-pause-circle\"></i> Pause";
        document.getElementById("startPause").style.backgroundColor = "#ef5350";
        document.getElementById("startPause").style.borderColor = "#ffffff";
    }
    else{
        running = 0;
        document.getElementById("start").innerHTML = "<i class=\"far fa-play-circle\"></i> Resume";
        document.getElementById("startPause").style.backgroundColor = "#4caf50";
        document.getElementById("startPause").style.borderColor = "#ffffff";
    }
}
function reset(){

    running = 0;
    time = 0;
    minutes=0;
    tenths=0;
    hours=0;
    document.getElementById("start").innerHTML = "<i class=\"far fa-play-circle\"></i> Start";
    document.getElementById("output").innerHTML = "00:00:00:0";
    document.getElementById("startPause").style.backgroundColor = "#3949AB";
    document.getElementById("startPause").style.borderColor = "#ffffff";

}
function increment(){
    if(running === 1){
        setTimeout (() => {
            minutes = Math.floor(time/10/60);
            seconds = Math.floor(time/10 % 60);
            hours = Math.floor(time/10/60/60);
            tenths = time % 10;
            time++;
            if(minutes < 10){
                minutes = "0" + minutes;
            }
            if(seconds < 10){
                seconds = "0" + seconds;
            }
            document.getElementById("output").innerHTML = "0" +hours + ":" + minutes + ":" + seconds + ":" + tenths ;
            increment();
        },100)
    }



}

