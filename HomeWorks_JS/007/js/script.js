const users = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
    {
        name: "Petr",
        surname: "Petrova",
        gender: "female",
        age: 22
    },
    {
        name: "Gogi",
        surname: "Sidorov",
        gender: "female",
        age: 27
    }];

const otherUsers = [{
    name: "Gogi",
    surname: "Ivanov",
    gender: "male",
    age: 33
},
    {
        name: "Givi",
        surname: "Petrova",
        gender: "female",
        age: 30
    }];


function excludeBy(firstArr, excludeArr, field) {
    return firstArr.filter(property =>{
        return !excludeArr.some(propertyOther => {
            return property[field]=== propertyOther[field];
        });
    });
}
let userChoice = prompt('Enter field', 'name');
console.log(excludeBy(users,otherUsers, userChoice));