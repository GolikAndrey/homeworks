/*ХРАНИЛИЩЕ ПИСЕМ*/
const mailStorage = [
    {
        subject: "Hello world",
        from: "gogidoe@somemail.nothing",
        to: "lolabola@ui.ux",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "How could you?!",
        from: "ladyboss@somemail.nothing",
        to: "ingeneer@nomail.here",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "Acces denied",
        from: "info@cornhub.com",
        to: "gogidoe@somemail.nothing",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    }
];

/*Конструкция $("#mailWrapper") - своего рода shortHand от document.querySelector('#mailWrapper'), позволяет нам быстрее обратиться к нужному елементу
* имя переменной начинается ос знака $ для того, чтобы различать в дальнейшем в коде какие данные являются jQuery объектами, а какие нет.
* Принципиальное различие закдючается в синтаксических различиях DOM дерева и его возмодностей от jQuery.*/
const $mailWrapper = $("#mailWrapper");

/*ФУНКЦИЯ createEmailItem. Будет отвечать за отрисовку ОДНОГО ПИСЬМА.
* Для того, чтобы можно было использовать ее и для письма с текстом и для письма без него, мы передаем два аргумнета:
*  1) emailFromStorage - это объект письма из хранилища, свойства которого будут помещены в текстовую составляющую верстки
*  2) showText - boolean флаг, который служит указателем для отображения или не отображения текста*/
const createEmailItem = (emailFromStorage, showText) => {
const $mailSliderItem = $('<div/>');
    /*СОЗДАЕМ ЕЛЕМЕНТ обертку для письма. Эта синтаксическая схема позволяет создать елемент, добавив к нему все нужные атрибуты,
    * при этом не создавая реальную разметку елемента, что достаточно удобно в плане читаемости
    * Все нудные атрибуты прописываем в формате `ключ: значение` в объект, которые передаем вторым параметром*/
    const $mailItem = $('<div/>', {
        class: 'mail-item',
    }); //переменная так же в имени помечается знаком $, т.к. она создана с помощью jQuery и на ней будут работать все методы библиотеки
    /*Так как нам нужно либо отображать текст либо нет, в зависимости от флага
    * при помощи тернарного оператора записываем либо готовый елемент разметки с текстом внутри, либо пустую строку*/
    const text = showText ? `<p class = "mail-item-text">${emailFromStorage.text}</p>`: "";

    /*в созданную обертку нужно вставить разметку с данными о письме. Применяем шаблонную строку,
    * так как разметка статичная*/
    $mailItem.append(
        `<h4 class="mail-item-subject">${emailFromStorage.subject}</h4>
     <p class="mail-item-from">${emailFromStorage.from}</p>
     <p class="mail-item-to">${emailFromStorage.to}</p>
     ${text}
     `);
    /*Результатом вызова функции будет это самое письмо в представлении елемента, обертки.
    * Внутри него уже будет нужная разметка с нужными данными.*/
    $mailSliderItem.append($mailItem);
    return $mailSliderItem;
};

/*ФНУКЦИЯ псевдо-переадрессации на другую страницу.
* Для того, чтобы отобразить полную информацию о письме на новой псевдо-странице, нужно это письмо получить
* Соответственно по этому оно и передается аргументом*/


/*ФУНКЦИЯ отображения списка писем.
* Аргументов она не требует и отвечает за отрисовку разметки со всеми письмами из хранилища*/
const showEmailList = () => {
    /*для того чтобы создать список и одним большим куском его поместить на страницу создаем фрагмент документа
    * переменная называется с пометкой $ так как мы обернули фрагмент документа в jQuery селектор. Делается это
    * для того, чтобы на фрагменте документа можно было вызывать методы jQuery*/
    const $documentFragment = $(document.createDocumentFragment());

    /*Для создания списка писем на странице нужно ДЛЯ КАЖДОГО письма из хранилища выполнить одну и ту же последовательность
    * действий, по этому встоеный метод массива .forEach() нам подходит. Он не изменяет исходный массив,
    * но выполняет код внутри колбэк функции для каждого елемента массива на котором вызывается.
    * В колбэк функции объявляем аргумент email, в который будет передаваться объект каждого письма на каждой итерации*/
    mailStorage.forEach((email) => {
        /*СОЗДАНИЕ ЕЛЕМЕНТА ПИСЬМА.
        * Присваиваем переменной $mailItem  результат вызова функции createEmailItem, агрументом которой передается
        * письмо (переменная email) и флаг false? т.к. отрисовывать текст для списка писем нам не нужно*/
        const $mailItem = createEmailItem(email, false);

        /*после того как елемент письма создан, нужно прикрепить к нему обработчик клика
        * с jQuery метод .click() передается колбэк функция, внутри которой будет происходить обработка клика
        * По клику на письмо нужно:
        *  - Показать текст. Если текст у даного письма уже открыт, то закрываем его и удаляем из разметки.
        *    Если же текст открыт в другом письме, то там его нужно закрыть и открыть в том, по которому был осуществлен клик
        *  - Если клик был по уже открытому тексту - перевести пользователя на псевдо-страницу с полной информацией о письме*/
        $mailItem.click((event) => {
            /*Записываем в переменную itemIndex индекс елемента-обертки письма по котрому был совершен клик.
            * jQuery метод .index() позволяет получить индекс елемента в его родительском елементе.
            * Так как порядок отображения писем на странице совпадает с порядком их нахождения в хранилище, И ТОЛЬКО ПО ЭТОМУ,
            * наша уловка сработает и мы сможем обратиться в хранилище с этим индексом и получить данные письма на которое было осуществлено нажатие*/
            const itemIndex = $(event.currentTarget).index();
            /*В зависимости от того открыт ли уже в письме текст, нужно будет либо удалить его, либо добавить.
            * По этому обращаемся с помощью jQuery селектора к разметке и пробуем получить елемент текста в том письме на которое было осуществлено нажатие.
            * РАЗШИФРОВКА СЕЛЕКТОРА:
            *     .mail-item:eq(${itemIndex}) - из коллекции елементов с классом `mail-item`, при помощи :eq(${itemIndex})
            *       будет выбран тот елемент, индекс которого соответствует значению в переменной itemIndex
            *      > .mail-item-text - среди дочерних елементов будут найдены ВСЕ с классом `mail-item-text`
            * из них мы возьмем первый он же [0]
            * Если текст не открыт в переменную запишется undefined, если текст открыт то запишется елемент с текстом*/
            let itemText = $(`.mail-item:eq(${itemIndex}) > .mail-item-text`)[0];

            /*undefined превратиться в false, в противном случае в переменной будет елемент и он трансформируется в true*/
            if(itemText) {
                /*если в переменной был не undefined нам нужно убедиться на что именно он нажал*/

                    $(itemText).remove(); //скрываем текст


            } else { //случай, когда в переменную itemText записан undefined
                /*ВО ВСЕХ елементах с классом mail-item заходим в ДОЧЕРНИЕ и среди них ищем елемент с классом mail-item-text.
                Затем удаляем найденные елементы, чтобы скрыть отображенные текст на странице*/
                $(`.mail-item > .mail-item-text`).remove();

                /*перезаписываем значение переменной itemText:
                * создаем елемент абзаца с нужными атрибутами согласно синаксису jQuery*/
                itemText = $('<p/>',{
                    class: 'mail-item-text',
                    text: mailStorage[itemIndex].text.slice(0,108) + "...", //обращаемся в хранилище к объекту под индексом itemIndex, у него вызываем свйоство text, чтобы "отрезать" с 0 до 108 символа и добавить в конце "...", чтобы не отображать НЕ ВЕСЬ ТЕКСТ
                });
                /*добавляем в письму, по которому было осуществлено нажатие получившийся елемент с текстом*/
                $(event.currentTarget).append(itemText);
            }
        });
        /*добавляем к фрагменту документа получившеся письмо*/
        $documentFragment.append($mailItem);
    });
    /*после выполнения цикла во фрагменте документа будет столько же писем с готовым функционалом, сколько объектов в хранилище
    * по этому смело вставляем их вовнутрь елемента $mailWrapper*/
    $mailWrapper.append($documentFragment);
};

/*при вызове функции showEmailList() будет отображен список писем на странице. Без этого вызова - ничего не произойдет.*/
showEmailList();

$(document).ready(() =>{
    $('#mailWrapper').slick({
        variableHeight: true,
        arrows: true,
        dots: true
    });
});

