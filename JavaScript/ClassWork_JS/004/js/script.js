function askTwoNumbers() { //объявляем функцию с именем askTwoNumbers без требуемых аргументов
    let start = Number(prompt("Enter numer start", "0")); //объявляем переменную start, значением которой будет результат приведения к цислу данных, введенных пользователем в модальное окно prompt
    let finish = Number(prompt("Enter numer finish", "10"));//объявляем переменную finish, значением которой будет результат приведения к цислу данных, введенных пользователем в модальное окно prompt

    while (start >= finish) { //объявляем цикл, если условие указанное в скобках будет истинным - попаем в тело цикла, если ложным - пропустим тело цикла и будет выполнятся код написанный ЗА ним
        start = Number(prompt("Enter correct number ")); //записываем в УЖЕ СУЩЕСТВУЮЩУЮ переменную start НОВЫЕ ДАННЫЕ от пользователя
        finish = Number(prompt("Enter correct finish number ")); //записываем в УЖЕ СУЩЕСТВУЮЩУЮ переменную finish НОВЫЕ ДАННЫЕ от пользователя
    }

    return {outerStart: start, outerFinish: finish,}; //все, что написано после ключевого слова return будет возвращено как РЕЗУЛЬТАТ ВЫЗОВА ФУНКЦИИ. В нашем случае функция после вызова вернет объект, в свойство outerStart которого будет записано ЗНАЧЕНИЕ переменной start, и в свойство outerFinish которого будет записано ЗНАЧЕНИЕ переменной finish
}

function askMeAgain(start, finish) { //объявляем функцию с именем askMeAgain, которой при вызове должны быть переданы два аргументаю Внутри функции значение первого аргумента будет доступно как переменная start, второго - как переменная finish

    let number = Number(prompt("Enter number","2")); //объявляем переменную start, значением которой будет результат приведения к цислу данных, введенных пользователем в модальное окно prompt

    for (start; start <= finish; start++){ //объявляем цикл с контролируемым количеством итераций. Переменной-бегунком которого будет переменная start; если условие start <= finish будет истинным - выполнится код в теле цикла, если ложным - пропустим тело цикла и будет выполнятся код написанный ЗА ним; после каждой итерации переменная-бегунок, она же start будет увеличиваться на 1.

        if(start % number === 0) { //проверяем величину остатка от деления start на number. Если остаток от деления start на number равен нулю то это значит что start кратен number.
            console.log('Number:',start) //выводим start в консоль если оно кратно number
        }
    }


}

////////////////И ТОЛЬКО ТУТ НАЧИНАЕТ ВЫПОЛНЯТЬСЯ ПРОГРАММА///////////////
/////////////ВСЕ ЧТО НАПИСАНО ДО ЭТОГО - ОБЪЯВЛЕНИЕ ФУНКЦИЙ///////////////

do { //ключевое слово, которое символизарует начало цикла do{}while(){}
    let twoNumbersObject = askTwoNumbers(); //объявляем переменную twoNumbersObject, значением которой будет результат вызова функции askTwoNumbers

    askMeAgain(twoNumbersObject['outerStart'], twoNumbersObject['outerFinish']); //вызываем функцию askMeAgain, которой первым параметром передаем ЗНАЧЕНИЕ СВОЙСТВА outerStart ОБЪЕКТА twoNumbersObject, вторым аргументом передаем ЗНАЧЕНИЕ СВОЙСТВА outerFinish ОБЪЕКТА twoNumbersObject

} while(confirm("Doy you ready to start again?")); //условием продолжения или прекращения выполнения кода в теле цикла служит результат вызова функции confirm, которая выводит пользователю модальное окно с выбором и возвращает true или false.
